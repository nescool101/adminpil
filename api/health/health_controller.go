package health

import (
	"github.com/gin-gonic/gin"
)

// GetController operation response processing function health
func GetController(c *gin.Context) {
	c.String(200, "pong")
}
