package mareigua

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/configapp"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

// Controller is struct for interface
type Controller struct {
	postMareiguaService IPostMareigua
	tracer              opentracing.Tracer
}

// NewPostMareiguaController contructor of controller
func NewPostMareiguaController(tracer opentracing.Tracer) *Controller {
	return &Controller{
		NewPostMareiguaService(),
		tracer}
}

// Controller Se encarga de realizar la validacion del mensaje de la peticion y respuesta
func (p *Controller) Controller(c *gin.Context) {
	span := logger.TracerLogParent(p.tracer, "Mareigua", c.Request.Header, c.Request.Method)

	data, _ := ioutil.ReadAll(c.Request.Body)
	var inputMsg interface{}
	_ = json.Unmarshal(data, &inputMsg)

	c.Request.Body = ioutil.NopCloser(bytes.NewReader(data))

	logger.TracerLog(span, "Mareigua-FRq", "", c.Request.URL, c.Request.Header, inputMsg, configapp.MaskMareiguaRq)

	var historicRq *modelsint.ReqBPostMareigua
	if err := c.ShouldBindJSON(&historicRq); err != nil {
		resp := globalmodels.BuildBadRequestError(err)
		c.Header("X-ApprolvaId", "0")
		c.Header(configapp.XRqUID, c.Request.Header.Get(configapp.XRqUID))
		logger.TracerLog(span, configapp.TagLogMareiguaFRs, "400", nil, c.Writer.Header(), resp, nil)

		c.JSON(http.StatusBadRequest, resp)
		return
	}

	var valHeaders modelsint.ReqHPostHeaders
	if errH := c.ShouldBindHeader(&valHeaders); errH != nil {
		resp := globalmodels.BuildBadRequestError(errH)
		c.Header("X-ApprolvaId", "0")
		c.Header(configapp.XRqUID, c.Request.Header.Get(configapp.XRqUID))
		logger.TracerLog(span, configapp.TagLogMareiguaFRs, "400", nil, c.Writer.Header(), resp, nil)

		c.JSON(http.StatusBadRequest, resp)
		return
	}

	var reqInfo globalmodels.Message

	reqInfo.HeaderReq = c.Request.Header
	reqInfo.Method = c.Request.Method

	response, resInfo, errorGeneric := p.postMareiguaService.PostMareiguaHandler(span, historicRq, reqInfo)

	for k, z := range resInfo.HeaderRes {
		c.Header(k, z)
	}

	if errorGeneric != nil {
		logger.TracerLog(span, configapp.TagLogMareiguaFRs, strconv.Itoa(resInfo.Status), nil, c.Writer.Header(), errorGeneric, nil)
		c.JSON(resInfo.Status, errorGeneric)
		return
	}

	logger.TracerLog(span, configapp.TagLogMareiguaFRs, strconv.Itoa(resInfo.Status), nil, c.Writer.Header(), response, nil)
	c.JSON(resInfo.Status, response)
}
