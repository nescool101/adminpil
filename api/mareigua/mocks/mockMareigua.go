package mocks

import (
	"net/http"
	"net/http/httptest"
	"time"
)

// ServerMock simula el backend de mareigua
func ServerMock() *httptest.Server {
	handler := http.NewServeMux()
	handler.HandleFunc("/token", usersMockToken)
	handler.HandleFunc("/tokenError", usersMockTokenError)
	handler.HandleFunc("/tokenTimeout", usersMockTokenTimeOut)
	handler.HandleFunc("/consultas", usersMockConsulta)
	handler.HandleFunc("/consultasError", usersMockConsultaError)
	handler.HandleFunc("/consultasTimeout", usersMockConsultaTimeOut)

	srv := httptest.NewServer(handler)

	return srv
}

func usersMockToken(w http.ResponseWriter, r *http.Request) {
	response := `{
					"access_token": "HrGW1H8NCE1BlWQLYPo6BSZD67U6yZpX-g4uB3gDKMBitrlfEjtf_AL16q7cFfqmCJwzXbP-iQaW3C3QEX9NBjGJRE2Cl_gLmdm4yguH7L30BfaUEFpvobvPIVxIR0vNpooV1IYU_59u0UD6qSa06I0WDcRrbOkjBAtrJSuMfq6bMzpXs_mZdKPF4w1-5gt68WFoL72Y88WFUjCp8aJXWscoMDi_4f8HcexK-bLKP7is6-mq3T1Mphf4sb-S1S9Hwfyex_-Rtzsf7-1LOA4TcxceqK6Ep6asPC3z-3jhOHDmrCc9DixZoq1zuRMfhvMzcx159R3TdGKixItWvysxMHYk6iEzm_AFdlwpG5_SKz6qARb8MxhL2tajDgJ4exqt",
					"token_type": "bearer",
					"expires_in": 604799
				}`
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(response))
}

func usersMockTokenError(w http.ResponseWriter, r *http.Request) {
	response := `{
		"error": "invalid_grant",
		"error_description": "Login o contraseña incorrecta"
	}`
	w.WriteHeader(http.StatusBadRequest)
	_, _ = w.Write([]byte(response))
}

func usersMockTokenTimeOut(w http.ResponseWriter, r *http.Request) {
	response := `{
		"error": "invalid_grant",
		"error_description": "Login o contraseña incorrecta"
	}`
	time.Sleep(15 * time.Second)
	w.WriteHeader(http.StatusBadRequest)
	_, _ = w.Write([]byte(response))
}

func usersMockConsulta(w http.ResponseWriter, r *http.Request) {
	response := `{
		"fecha": "6/2/2020 2:31:30 PM",
		"consulta_id": 1234,
		"respuesta_id": 4,
		"tipo_identificacion_persona_natural_id": 1,
		"numero_identificacion_persona_natural": "10083463",
		"primer_nombre_persona_natural": "PrimerNombre201",
		"segundo_nombre_persona_natural": "SegundoNombre201",
		"primer_apellido_persona_natural": "PrimerApellido201",
		"segundo_apellido_persona_natural": "SegundoApellido201",
		"EPS": "null",
		"AFP": "null",
		"aportantes": [
			{
				"tipo_identificacion_aportante_id": 3,
				"numero_identificacion_aportante": "432543",
				"razón_social_aportante": "Empresa1",
				"tipo_cotizante_persona_natural": 1,
				"tiene_salario_integral_actualmente": false,
				"resultado_pagos": [
					{
						"ano_periodo_validado": 2020,
						"mes_periodo_validado": 5,
						"realizo_pago": true
					}
				]
			}
		],
		"resultado_salario": true,
		"resultado_salario_zona_gris": false,
		"rango_salario": "menor a -40%"
	}`
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(response))
}

func usersMockConsultaError(w http.ResponseWriter, r *http.Request) {
	response := `{
		"Message": "Authorization has been denied for this request."
	}`
	w.WriteHeader(http.StatusUnauthorized)
	_, _ = w.Write([]byte(response))
}

func usersMockConsultaTimeOut(w http.ResponseWriter, r *http.Request) {
	response := `{
		"Message": "Authorization has been denied for this request."
	}`
	time.Sleep(15 * time.Second)
	w.WriteHeader(http.StatusUnauthorized)
	_, _ = w.Write([]byte(response))
}
