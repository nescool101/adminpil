package mareigua

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/configapp"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsext"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/commons"
	"github.com/avaldigitallabs/adl-sc-backend-library/config"
	"github.com/avaldigitallabs/adl-sc-backend-library/date"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
)

// PostMareigua is struct for service
type PostMareigua struct {
}

// NewPostMareiguaService contructor of service
func NewPostMareiguaService() IPostMareigua {
	return &PostMareigua{}
}

// PostMareiguaHandler funcion de negocio para la consulta de mareigua
func (p *PostMareigua) PostMareiguaHandler(span context.Context,
	historicRq *modelsint.ReqBPostMareigua, message globalmodels.Message) (*modelsint.ResBPostMareigua,
	*globalmodels.Message, *globalmodels.ErrorGeneric) {
	// Headers de respuesta
	message.HeaderRes = map[string]string{
		"Content-Type": "application/json",
		"X-RqUID":      message.HeaderReq.Get("X-RqUID"),
		"X-ApprovalId": "0",
	}

	// Esquemas utilizados en el backend
	var tokenOauthMareiguaRs modelsext.ResBPostOauthMareigua
	var tokenOauthMareiguaError modelsext.ResBMareiguaError
	var queryMareiguaRq modelsext.ReqBPostQueryMareigua
	var queryMareiguaRs modelsext.ResBPostQueryMareigua

	responseToken, message, errorToken := getToken(span, historicRq, message)
	if errorToken != nil {
		return nil, &message, errorToken
	}
	defer responseToken.Body.Close()
	// si se obtiene el token
	if responseToken.StatusCode == 200 {
		_ = json.NewDecoder(responseToken.Body).Decode(&tokenOauthMareiguaRs)

		// Log token PRs
		logger.TracerLog(span,
			"MareiguaToken-PRs",
			strconv.Itoa(responseToken.StatusCode),
			nil,
			responseToken.Header,
			tokenOauthMareiguaRs,
			nil)

		// headers
		headersQuery := http.Header{}
		headersQuery.Add(configapp.ContentType,
			configapp.ContentTypeJSON)
		headersQuery.Add("Authorization",
			tokenOauthMareiguaRs.TokenType+" "+tokenOauthMareiguaRs.AccessToken)

		// url de consumo
		urlQuery, err := url.Parse(os.Getenv("URL_QUERYMAREIGUA"))

		if err != nil {
			message.Status = http.StatusInternalServerError
			message.Description = configapp.MsgInternalError

			errorGeneric := globalmodels.BuildInternalServerError(err)

			return nil, &message, &errorGeneric
		}

		// Arma mensaje para realizar la consulta
		queryMareiguaRq.TipoIdentificacionID = configapp.DocumentTypeRq[message.HeaderReq.Get("X-GovIssueIdentType")]
		queryMareiguaRq.NumeroEmpleadoID = message.HeaderReq.Get("X-IdentSerialNum")
		queryMareiguaRq.SalarioMensual = historicRq.CurAmt.Amt
		product, _ := strconv.Atoi(historicRq.RefInfo.RefType)
		queryMareiguaRq.ProductoID = product

		dataQuery, _ := json.Marshal(queryMareiguaRq)

		logger.TracerLog(span, "MareiguaQuery-PRq", "", urlQuery, headersQuery, queryMareiguaRq, nil)

		// timeout
		timeOut := date.TimeoutValue(os.Getenv("TIMEOUT_BACKEND"))

		responseQuery, err := commons.ClientHTTP(dataQuery, headersQuery, urlQuery, "POST", timeOut)

		if err != nil && responseQuery == nil {
			var errorGeneric globalmodels.ErrorGeneric
			if strings.Contains(err.Error(), "Client.Timeout") {
				message.Status = http.StatusGatewayTimeout
				message.Description = configapp.TimeoutDescription
				errorGeneric = globalmodels.BuildTimeOutError(err)
			} else {
				message.Status = http.StatusInternalServerError
				message.Description = config.MsgInternalError
				errorGeneric = globalmodels.BuildInternalServerError(err)
			}
			return nil, &message, &errorGeneric
		}
		defer responseQuery.Body.Close()
		if responseQuery.StatusCode == 200 {
			_ = json.NewDecoder(responseQuery.Body).Decode(&queryMareiguaRs)
			// Log query PRs
			logger.TracerLog(span,
				"MareiguaQuery-PRs",
				strconv.Itoa(responseQuery.StatusCode),
				nil,
				responseQuery.Header,
				queryMareiguaRs,
				nil)

			resQueryMap, message, errorGeneric := resQueryMap(&queryMareiguaRs, message)

			return resQueryMap, message, errorGeneric
		}

		_ = json.NewDecoder(responseQuery.Body).Decode(&tokenOauthMareiguaError)

		// Log query PRs
		logger.TracerLog(span, "MareiguaQuery-PRs", strconv.Itoa(responseQuery.StatusCode),
			nil, responseQuery.Header, tokenOauthMareiguaError, nil)

		long := len(responseQuery.Status)

		message.Status = http.StatusPartialContent

		var statusInfo globalmodels.Status
		statusInfo.StatusCode = strconv.Itoa(responseQuery.StatusCode)
		statusInfo.StatusDesc = responseQuery.Status[4:long]
		statusInfo.Severity = configapp.MsgSeverityError
		statusInfo.ServerStatusCode = strconv.Itoa(responseQuery.StatusCode)
		statusInfo.ServerStatusDesc = tokenOauthMareiguaError.Message
		statusInfo.EndDt = date.DateNow()

		var additionalInfo globalmodels.AdditionalStatus
		additionalInfo.StatusCode = strconv.Itoa(responseQuery.StatusCode)
		additionalInfo.StatusDesc = responseQuery.Status[4:long]
		additionalInfo.Severity = configapp.MsgSeverityError
		additionalInfo.ServerStatusCode = strconv.Itoa(responseQuery.StatusCode)
		additionalInfo.ServerStatusDesc = tokenOauthMareiguaError.Message
		additionalInfo.EndDt = date.DateNow()

		errorGeneric := globalmodels.BuildError(statusInfo, additionalInfo)

		return nil, &message, &errorGeneric
	}

	_ = json.NewDecoder(responseToken.Body).Decode(&tokenOauthMareiguaError)

	// Log token PRs
	logger.TracerLog(span,
		"MareiguaToken-PRs",
		strconv.Itoa(responseToken.StatusCode),
		nil,
		responseToken.Header,
		tokenOauthMareiguaError,
		nil)

	long := len(responseToken.Status)

	var statusInfo globalmodels.Status
	statusInfo.StatusCode = strconv.Itoa(responseToken.StatusCode)
	statusInfo.StatusDesc = responseToken.Status[4:long]
	statusInfo.Severity = configapp.MsgSeverityError
	statusInfo.ServerStatusCode = tokenOauthMareiguaError.Error
	statusInfo.ServerStatusDesc = tokenOauthMareiguaError.ErrorDescription
	statusInfo.EndDt = date.DateNow()

	var additionalInfo globalmodels.AdditionalStatus
	additionalInfo.StatusCode = strconv.Itoa(responseToken.StatusCode)
	additionalInfo.StatusDesc = responseToken.Status[4:long]
	additionalInfo.Severity = configapp.MsgSeverityError
	additionalInfo.ServerStatusCode = tokenOauthMareiguaError.Error
	additionalInfo.ServerStatusDesc = tokenOauthMareiguaError.ErrorDescription
	additionalInfo.EndDt = date.DateNow()

	message.Status = responseToken.StatusCode

	errorGeneric := globalmodels.BuildError(statusInfo, additionalInfo)

	return nil, &message, &errorGeneric
}

// getToken obtiene el token de mareigua
func getToken(span context.Context, historicRq *modelsint.ReqBPostMareigua,
	message globalmodels.Message) (*http.Response,
	globalmodels.Message,
	*globalmodels.ErrorGeneric) {
	// headers
	headersToken := http.Header{}
	headersToken.Add(configapp.ContentType, configapp.ContentTypeForm)

	// Peticion para obtener el token
	var tokenOauthMareiguaRq modelsext.ReqBPostOauthMareigua

	tokenOauthMareiguaRq.GrantType = url.QueryEscape(historicRq.SecretList.CryptType)
	tokenOauthMareiguaRq.Password = url.QueryEscape(historicRq.SecretList.SecretID)
	tokenOauthMareiguaRq.Username = url.QueryEscape(historicRq.SecretList.Secret)

	oauthRq := "grant_type=" + tokenOauthMareiguaRq.GrantType + "&username=" +
		tokenOauthMareiguaRq.Username + "&password=" + tokenOauthMareiguaRq.Password
	body := []byte(oauthRq)

	// url de consumo
	urlToken, err := url.Parse(os.Getenv("URL_TOKENMAREIGUA"))

	if err != nil {
		message.Status = http.StatusInternalServerError
		errorGeneric := globalmodels.BuildInternalServerError(err)

		return nil, message, &errorGeneric
	}

	timeOut := date.TimeoutValue(os.Getenv("TIMEOUT_BACKEND"))

	logger.TracerLog(span, "MareiguaToken-PRq", "", urlToken, headersToken, tokenOauthMareiguaRq, configapp.MaskBTokenMareiguaRs)
	responseToken, err := commons.ClientHTTP(body, headersToken, urlToken, "POST", timeOut)

	if err != nil && responseToken == nil {
		var errorGeneric globalmodels.ErrorGeneric
		if strings.Contains(err.Error(), "Client.Timeout") {
			message.Status = http.StatusGatewayTimeout
			message.Description = configapp.TimeoutDescription
			errorGeneric = globalmodels.BuildTimeOutError(err)
		} else {
			message.Status = http.StatusInternalServerError
			message.Description = config.MsgInternalError
			errorGeneric = globalmodels.BuildInternalServerError(err)
		}
		return nil, message, &errorGeneric
	}
	return responseToken, message, nil
}

// resQueryMap arma la respuesta de mareigua cuando la respuesta es exitosa
func resQueryMap(queryMareiguaRs *modelsext.ResBPostQueryMareigua,
	message globalmodels.Message) (*modelsint.ResBPostMareigua,
	*globalmodels.Message,
	*globalmodels.ErrorGeneric) {
	if queryMareiguaRs.RespuestaID == 4 {
		var historicRs modelsint.ResBPostMareigua
		var personInfo modelsint.PersonInfo

		var govIssueIdent modelsint.GovIssueIdent

		govIssueIdent.GovIssueIdentType = configapp.DocumentTypeRs[queryMareiguaRs.TipoIdentificacionPersonaNaturalID]
		govIssueIdent.IdentSerialNum = queryMareiguaRs.NumeroIdentificacionPersonaNatural

		personInfo.GovIssueIdent = &govIssueIdent

		var personName modelsint.PersonName
		personName.FirstName = queryMareiguaRs.PrimerNombrePersonaNatural
		personName.MiddleName = queryMareiguaRs.SegundoNombrePersonaNatural
		personName.LastName = queryMareiguaRs.PrimerApellidoPersonaNatural
		personName.SecondLastName = queryMareiguaRs.SegundoApellidoPersonaNatural

		personInfo.PersonName = &personName

		var socialSecurityInfo modelsint.SocialSecurityInfo
		socialSecurityInfo.EPSName = queryMareiguaRs.EPS
		socialSecurityInfo.AFPName = queryMareiguaRs.AFP

		personInfo.SocialSecurityInfo = &socialSecurityInfo

		historicRs.PersonInfo = &personInfo

		var contributions []modelsint.Contribution
		for _, v := range queryMareiguaRs.Aportantes {
			var contribution modelsint.Contribution

			var govIssueIdent modelsint.GovIssueIdent
			govIssueIdent.GovIssueIdentType = configapp.DocumentTypeRs[v.TipoIdentificacionAportanteID]
			govIssueIdent.IdentSerialNum = v.NumeroIdentificacionAportante

			contribution.GovIssueIdent = &govIssueIdent

			var personName modelsint.PersonName
			personName.FullName = v.RazónSocialAportante

			contribution.PersonName = &personName
			contribution.IntegralSalary = v.TieneSalarioIntegralActualmente

			var contributionData modelsint.ContributionData
			contributionData.Code = strconv.Itoa(v.TipoCotizantePersonaNatural)

			contribution.ContributionData = &contributionData

			var contributionPeriods []modelsint.ContributionPeriods
			for _, c := range v.ResultadoPagos {
				var contributionPeriod modelsint.ContributionPeriods

				contributionPeriod.ExpYear = strconv.Itoa(c.AnoPeriodoValidado)
				contributionPeriod.ExpMonth = strconv.Itoa(c.MesPeriodoValidado)
				contributionPeriod.PaymentInd = c.RealizoPago

				var curAmt modelsint.CurAmt
				curAmt.Amt = c.Ingresos
				contributionPeriod.CurAmt = &curAmt

				contributionPeriods = append(contributionPeriods, contributionPeriod)
			}
			contribution.ContributionPeriods = contributionPeriods

			var averageCurAmt modelsint.CurAmt
			averageCurAmt.Amt = v.PromedioIngresos
			contribution.AverageCurAmt = &averageCurAmt

			var mediamCurAmt modelsint.CurAmt
			mediamCurAmt.Amt = v.MediaIngresos
			contribution.MediamCurAmt = &mediamCurAmt

			contributions = append(contributions, contribution)
		}

		historicRs.Contribution = contributions

		var salaryValidation modelsint.SalaryValidation

		if queryMareiguaRs.ResultadoSalario != nil || queryMareiguaRs.ResultadoSalarioZonaGris != nil || queryMareiguaRs.RangoSalario != "" {
			salaryValidation.SalaryResult = queryMareiguaRs.ResultadoSalario
			salaryValidation.GrayZoneSalaryResult = queryMareiguaRs.ResultadoSalarioZonaGris
			salaryValidation.SalaryRange = queryMareiguaRs.RangoSalario

			historicRs.SalaryValidation = &salaryValidation
		}
		message.Status = http.StatusCreated
		return &historicRs, &message, nil
	}

	message.Status = http.StatusPartialContent
	var statusInfo globalmodels.Status
	statusInfo.StatusCode = strconv.Itoa(queryMareiguaRs.RespuestaID)
	statusInfo.StatusDesc = configapp.ErrorDescription[queryMareiguaRs.RespuestaID]
	statusInfo.Severity = configapp.MsgSeverityError
	statusInfo.ServerStatusCode = strconv.Itoa(queryMareiguaRs.RespuestaID)
	statusInfo.ServerStatusDesc = configapp.ErrorDescription[queryMareiguaRs.RespuestaID]
	statusInfo.EndDt = date.DateNow()

	var additionalInfo globalmodels.AdditionalStatus
	additionalInfo.StatusCode = strconv.Itoa(queryMareiguaRs.RespuestaID)
	additionalInfo.StatusDesc = configapp.ErrorDescription[queryMareiguaRs.RespuestaID]
	additionalInfo.Severity = configapp.MsgSeverityError
	additionalInfo.ServerStatusCode = strconv.Itoa(queryMareiguaRs.RespuestaID)
	additionalInfo.ServerStatusDesc = configapp.ErrorDescription[queryMareiguaRs.RespuestaID]
	additionalInfo.EndDt = date.DateNow()

	errorGeneric := globalmodels.BuildError(statusInfo, additionalInfo)

	return nil, &message, &errorGeneric
}
