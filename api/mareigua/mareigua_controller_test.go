package mareigua

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/api/mareigua/mocks"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
	"github.com/gin-gonic/gin"

	"github.com/stretchr/testify/assert"
)

func TestPostHistoricController_InternalServer(t *testing.T) {

	//var response models.HistoricRs
	var message globalmodels.Message
	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	serviceMock := new(mocks.IPostMareigua)

	serviceMock.On("PostHistoricController", request).Return(nil, request, message)

	controller := NewPostMareiguaController(tracer)

	b, _ := json.Marshal(request)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/Mareigua", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	req.Header.Add("X-CompanyId", "0001")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/Mareigua", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusInternalServerError, w.Code)

}

func TestPostHistoricController_BadRequestHeaders(t *testing.T) {

	var message globalmodels.Message
	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	serviceMock := new(mocks.IPostMareigua)

	serviceMock.On("PostHistoricController", request).Return(nil, request, message)

	controller := NewPostMareiguaController(tracer)

	b, _ := json.Marshal(request)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/Mareigua", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/Mareigua", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)

}

func TestPostHistoricController_BadRequestBody(t *testing.T) {

	var message globalmodels.Message
	var request modelsint.ReqBPostMareigua

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	serviceMock := new(mocks.IPostMareigua)

	serviceMock.On("PostHistoricController", request).Return(nil, request, message)

	controller := NewPostMareiguaController(tracer)

	b, _ := json.Marshal(`{
		"SecretList": {
		  
		  "CryptType": "password",
	  
		},
		"RefInfo": {
		  "RefType": "4"
		}
	  }`)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/Mareigua", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	req.Header.Add("X-CompanyId", "0001")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/Mareigua", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)

}

func TestPostHistoricController_OK(t *testing.T) {

	srv := mocks.ServerMock()
	defer srv.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "8")
	_ = os.Setenv("URL_TOKENMAREIGUA", srv.URL+"/token")
	_ = os.Setenv("URL_QUERYMAREIGUA", srv.URL+"/consultas")

	var message globalmodels.Message
	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	serviceMock := new(mocks.IPostMareigua)

	serviceMock.On("PostHistoricController", request).Return(nil, request, message)

	controller := NewPostMareiguaController(tracer)

	b, _ := json.Marshal(request)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/Mareigua", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	req.Header.Add("X-CompanyId", "0001")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/Mareigua", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)

}
