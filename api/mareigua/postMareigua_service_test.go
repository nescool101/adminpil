package mareigua

import (
	"net/http"
	"os"
	"testing"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/api/mareigua/mocks"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
)

func Test_getTokenAndQueryOK(t *testing.T) {
	srv := mocks.ServerMock()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("test_Administration")
	defer closer.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "8")
	_ = os.Setenv("URL_TOKENMAREIGUA", srv.URL+"/token")
	_ = os.Setenv("URL_QUERYMAREIGUA", srv.URL+"/consultas")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	mareigua := NewPostMareiguaService()

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	var reqInfo globalmodels.Message

	headers := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "Mareigua", headers, "POST")

	reqInfo.HeaderReq = headers
	reqInfo.Method = "POST"

	_, resInfo, errorGeneral := mareigua.PostMareiguaHandler(span, &request, reqInfo)
	if errorGeneral != nil {
		t.Error(errorGeneral)
	}

	if http.StatusCreated != resInfo.Status {
		t.Error("expected", http.StatusCreated, "got", resInfo.Status)
	}
}

func Test_getTokenInvalid(t *testing.T) {
	srv := mocks.ServerMock()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("test_Administration")
	defer closer.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "8")
	_ = os.Setenv("URL_TOKENMAREIGUA", srv.URL+"/tokenError")
	_ = os.Setenv("URL_QUERYMAREIGUA", srv.URL+"/consultas")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	mareigua := NewPostMareiguaService()

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	var reqInfo globalmodels.Message

	headers := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "Mareigua", headers, "POST")

	reqInfo.HeaderReq = headers
	reqInfo.Method = "POST"

	_, resInfo, _ := mareigua.PostMareiguaHandler(span, &request, reqInfo)

	if http.StatusBadRequest != resInfo.Status {
		t.Error("expected", http.StatusBadRequest, "got", resInfo.Status)
	}
}

func Test_getQueryInvalid(t *testing.T) {
	srv := mocks.ServerMock()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("test_Administration")
	defer closer.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "8")
	_ = os.Setenv("URL_TOKENMAREIGUA", srv.URL+"/token")
	_ = os.Setenv("URL_QUERYMAREIGUA", srv.URL+"/consultasError")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	mareigua := NewPostMareiguaService()

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	var reqInfo globalmodels.Message

	headers := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "Mareigua", headers, "POST")

	reqInfo.HeaderReq = headers
	reqInfo.Method = "POST"

	_, resInfo, _ := mareigua.PostMareiguaHandler(span, &request, reqInfo)

	if http.StatusPartialContent != resInfo.Status {
		t.Error("expected", http.StatusPartialContent, "got", resInfo.Status)
	}
}

func Test_getQueryTimeOut(t *testing.T) {
	srv := mocks.ServerMock()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("test_Administration")
	defer closer.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "2")
	_ = os.Setenv("URL_TOKENMAREIGUA", srv.URL+"/token")
	_ = os.Setenv("URL_QUERYMAREIGUA", srv.URL+"/consultasTimeout")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	mareigua := NewPostMareiguaService()

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	var reqInfo globalmodels.Message

	headers := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "Mareigua", headers, "POST")

	reqInfo.HeaderReq = headers
	reqInfo.Method = "POST"

	_, resInfo, _ := mareigua.PostMareiguaHandler(span, &request, reqInfo)

	if http.StatusGatewayTimeout != resInfo.Status {
		t.Error("expected", http.StatusGatewayTimeout, "got", resInfo.Status)
	}
}

func Test_getTokenTimeOut(t *testing.T) {
	srv := mocks.ServerMock()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("test_Administration")
	defer closer.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "2")
	_ = os.Setenv("URL_TOKENMAREIGUA", srv.URL+"/tokenTimeout")
	_ = os.Setenv("URL_QUERYMAREIGUA", srv.URL+"/consultas")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	mareigua := NewPostMareiguaService()

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	var reqInfo globalmodels.Message

	headers := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "Mareigua", headers, "POST")

	reqInfo.HeaderReq = headers
	reqInfo.Method = "POST"

	_, resInfo, _ := mareigua.PostMareiguaHandler(span, &request, reqInfo)

	if http.StatusGatewayTimeout != resInfo.Status {
		t.Error("expected", http.StatusGatewayTimeout, "got", resInfo.Status)
	}
}

func Test_getTokenErrorUrl(t *testing.T) {
	srv := mocks.ServerMock()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("test_Administration")
	defer closer.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "8")
	_ = os.Setenv("URL_TOKENMAREIGUA", "")
	_ = os.Setenv("URL_QUERYMAREIGUA", srv.URL+"/consultasTimeout")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	mareigua := NewPostMareiguaService()

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	var reqInfo globalmodels.Message

	headers := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "Mareigua", headers, "POST")

	reqInfo.HeaderReq = headers
	reqInfo.Method = "POST"

	_, resInfo, _ := mareigua.PostMareiguaHandler(span, &request, reqInfo)

	if http.StatusInternalServerError != resInfo.Status {
		t.Error("expected", http.StatusInternalServerError, "got", resInfo.Status)
	}
}

func Test_getQueryErrorUrl(t *testing.T) {
	srv := mocks.ServerMock()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("test_Administration")
	defer closer.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "8")
	_ = os.Setenv("URL_TOKENMAREIGUA", srv.URL+"/token")
	_ = os.Setenv("URL_QUERYMAREIGUA", "")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	mareigua := NewPostMareiguaService()

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	var reqInfo globalmodels.Message

	headers := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "Mareigua", headers, "POST")

	reqInfo.HeaderReq = headers
	reqInfo.Method = "POST"

	_, resInfo, _ := mareigua.PostMareiguaHandler(span, &request, reqInfo)

	if http.StatusInternalServerError != resInfo.Status {
		t.Error("expected", http.StatusInternalServerError, "got", resInfo.Status)
	}
}
