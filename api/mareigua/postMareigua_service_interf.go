package mareigua

import (
	"context"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
)

// IPostMareigua interface for service PostMareigua
type IPostMareigua interface {
	PostMareiguaHandler(span context.Context,
		historicRq *modelsint.ReqBPostMareigua,
		message globalmodels.Message) (*modelsint.ResBPostMareigua,
		*globalmodels.Message,
		*globalmodels.ErrorGeneric)
}
