package soi

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/configapp"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsext"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/commons"
	"github.com/avaldigitallabs/adl-sc-backend-library/config"
	"github.com/avaldigitallabs/adl-sc-backend-library/date"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
)

// PostSOI is struct for service
type PostSOI struct {
}

// NewPostSOIService contructor of service
func NewPostSOIService() IPostSOI {
	return &PostSOI{}
}

// PostSOIHandler funcion de negocio para la consulta de soi
func (p *PostSOI) PostSOIHandler(span context.Context,
	historicRq *modelsint.ReqBPostSOI, message globalmodels.Message) (*modelsint.ResBPostSOI,
	*globalmodels.Message, *globalmodels.ErrorGeneric) {
	// Headers de respuesta
	message.HeaderRes = map[string]string{
		"Content-Type":   "application/json",
		configapp.XRqUID: message.HeaderReq.Get(configapp.XRqUID),
		"X-ApprovalId":   "0",
	}

	responseToken, message, errorToken := getToken(span, historicRq, message)
	if errorToken != nil {
		return nil, &message, errorToken
	}

	// si se obtiene el token
	if responseToken.StatusCode == 200 {
		// Esquemas utilizados en el backend
		var tokenOauthSOIRs modelsext.ResBPostOauthSOI
		_ = json.NewDecoder(responseToken.Body).Decode(&tokenOauthSOIRs)

		// Log token PRs
		logger.TracerLog(span, "SOIToken-PRs",
			strconv.Itoa(responseToken.StatusCode), nil,
			responseToken.Header, tokenOauthSOIRs, nil)

		responseQuery, data, errorToken := postQuery(span, historicRq, responseToken.Header.Get("Set-Cookie"), message)
		if errorToken != nil {
			return nil, &data, errorToken
		}
		// si la consulta es exitosa
		if responseQuery.StatusCode == 200 {
			var QuerySOIRs []modelsext.ResBPostQuerySOI
			var resBPostSOI modelsint.ResBPostSOI
			_ = json.NewDecoder(responseQuery.Body).Decode(&QuerySOIRs)

			var financialInformations []modelsint.FinancialInformation
			for x := range QuerySOIRs {
				v := QuerySOIRs[x]
				var financialInformation modelsint.FinancialInformation
				var personName modelsint.PersonName
				personName.FirstName = v.PrimerNombre
				personName.MiddleName = v.SegundoNombre
				personName.LastName = v.PrimerApellido
				personName.SecondLastName = v.SegundoApellido

				var personInfo modelsint.PersonInfo
				personInfo.PersonName = &personName

				var govIssueIdent modelsint.GovIssueIdent

				govIssueIdent.GovIssueIdentType = v.TipoDocumentoUsuario
				govIssueIdent.IdentSerialNum = v.NumeroDocumentoUsuario

				personInfo.GovIssueIdent = &govIssueIdent
				financialInformation.PersonInfo = &personInfo

				var orgInfo modelsint.OrgInfo
				orgInfo.Name = v.RazonSocial
				financialInformation.OrgInfo = &orgInfo

				var orgID modelsint.OrgID

				orgID.OrgIDType = v.TipoDocumentoAportante
				orgID.OrgIDNum = v.NumeroDocumentoAportante
				financialInformation.OrgID = &orgID

				financialInformation.Code = v.CodClaseAportante
				financialInformation.Value = v.DesClaseAportante

				var businessActivity modelsint.BusinessActivity
				businessActivity.BusinessType = v.CodActividadEconomica
				businessActivity.Desc = v.DesActividadEconomica
				businessActivity.Income = v.Ingreso
				businessActivity.Expenses = v.Retiro

				financialInformation.BusinessActivity = &businessActivity

				financialInformation.Issuer = v.CodTipoCotizante
				financialInformation.Desc = v.DescTipoCotizante
				financialInformation.EstablishDt = v.PeriodoCotizacion
				var curAmt modelsint.CurAmt
				curAmt.Amt = v.SalarioBasico

				financialInformation.CurAmt = &curAmt

				if v.SalarioIntegral == "" {
					financialInformation.Amt = 0
				} else {
					financialInformation.Amt, _ = strconv.Atoi(v.SalarioIntegral)
				}
				additionalData := []modelsint.AdditionalData{
					{
						Name:  "tipoCotizante",
						Value: v.DescTipoCotizante,
					},
					{
						Name:  "tipoPlanillacodigo",
						Value: v.CodTipoPlanilla,
					},
					{
						Name:  "tipoPlanillaDescripcion",
						Value: v.DescTipoPlanilla,
					},
					{
						Name:  "ingresoBaseCotizacionPension",
						Value: strconv.Itoa(v.IngresoBaseCotizacionPension),
					},
					{
						Name:  "ingresoBaseCotizacionSalud",
						Value: strconv.Itoa(v.IngresoBaseCotizacionSalud),
					},
					{
						Name:  "ingresoBaseCotizacionSubsidioFamiliar",
						Value: strconv.Itoa(v.IngresoBaseCotizacionSubFamiliar),
					},
				}

				financialInformation.AdditionalData = additionalData

				financialInformations = append(financialInformations, financialInformation)
			}
			resBPostSOI.FinancialInformation = financialInformations
			message.Status = 201

			go logout(span, historicRq, responseToken.Header.Get("Set-Cookie"), message)

			return &resBPostSOI, &message, nil
		}

		data, errorGeneric := errorResponse(span, responseQuery, message, "SOIQuery-PRs")
		return nil, &data, &errorGeneric
	}

	message, errorGeneric := errorResponse(span, responseToken, message, "SOIToken-PRs")
	return nil, &message, &errorGeneric
}

// errorResponse construye el error dependiendo de la respusta
func errorResponse(span context.Context, response *http.Response, message globalmodels.Message,
	nameLog string) (globalmodels.Message, globalmodels.ErrorGeneric) {
	var errorGeneric globalmodels.ErrorGeneric
	var ResBSOIError modelsext.ResBSOIError
	long := len(response.Status)

	resp := json.NewDecoder(response.Body)
	_ = resp.Decode(&ResBSOIError)

	// Log token PRs
	logger.TracerLog(span, nameLog,
		strconv.Itoa(response.StatusCode), nil,
		response.Header, ResBSOIError, nil)

	if ResBSOIError.Message != "" {
		message.Status = http.StatusPartialContent

		message.Status = http.StatusPartialContent
		var statusInfo globalmodels.Status
		statusInfo.StatusCode = strconv.Itoa(response.StatusCode)
		statusInfo.StatusDesc = response.Status[4:long]
		statusInfo.Severity = configapp.MsgSeverityError
		statusInfo.ServerStatusCode = strconv.Itoa(response.StatusCode)
		statusInfo.ServerStatusDesc = ResBSOIError.Message
		statusInfo.EndDt = date.DateNow()

		var additionalInfo globalmodels.AdditionalStatus
		additionalInfo.StatusCode = strconv.Itoa(response.StatusCode)
		additionalInfo.StatusDesc = response.Status[4:long]
		additionalInfo.Severity = configapp.MsgSeverityError
		additionalInfo.ServerStatusCode = strconv.Itoa(response.StatusCode)
		additionalInfo.ServerStatusDesc = ResBSOIError.Message
		additionalInfo.EndDt = date.DateNow()

		errorGeneric = globalmodels.BuildError(statusInfo, additionalInfo)
	} else {
		if ResBSOIError.StatusCode != "" {
			message.Status = http.StatusInternalServerError

			var statusInfo globalmodels.Status
			statusInfo.StatusCode = ResBSOIError.StatusCode
			statusInfo.StatusDesc = ResBSOIError.StatusDesc
			statusInfo.Severity = configapp.MsgSeverityError
			statusInfo.ServerStatusCode = ResBSOIError.ServerStatusCode
			statusInfo.ServerStatusDesc = ResBSOIError.ServerStatusDesc
			statusInfo.EndDt = date.DateNow()

			var additionalInfo globalmodels.AdditionalStatus
			additionalInfo.StatusCode = ResBSOIError.StatusCode
			additionalInfo.StatusDesc = ResBSOIError.StatusDesc
			additionalInfo.Severity = configapp.MsgSeverityError
			additionalInfo.ServerStatusCode = ResBSOIError.ServerStatusCode
			additionalInfo.ServerStatusDesc = "Name MPG: " + ResBSOIError.DPObject + " URLIN: " +
				ResBSOIError.URLIn + " URLOUT: " + ResBSOIError.URLOut
			additionalInfo.EndDt = date.DateNow()

			errorGeneric = globalmodels.BuildError(statusInfo, additionalInfo)
		} else {
			message.Status = http.StatusServiceUnavailable
			var statusInfo globalmodels.Status
			statusInfo.StatusCode = strconv.Itoa(http.StatusServiceUnavailable)
			statusInfo.StatusDesc = configapp.DescServiceUna
			statusInfo.Severity = configapp.MsgSeverityError
			statusInfo.ServerStatusCode = strconv.Itoa(http.StatusServiceUnavailable)
			statusInfo.ServerStatusDesc = configapp.DescServiceError
			statusInfo.EndDt = date.DateNow()

			var additionalInfo globalmodels.AdditionalStatus
			additionalInfo.StatusCode = strconv.Itoa(http.StatusServiceUnavailable)
			additionalInfo.StatusDesc = configapp.DescServiceUna
			additionalInfo.Severity = configapp.MsgSeverityError
			additionalInfo.ServerStatusCode = strconv.Itoa(http.StatusServiceUnavailable)
			additionalInfo.ServerStatusDesc = configapp.DescServiceError
			additionalInfo.EndDt = date.DateNow()

			errorGeneric = globalmodels.BuildError(statusInfo, additionalInfo)
		}
	}

	return message, errorGeneric
}

// getToken obtiene el token de SOI
func getToken(span context.Context, historicRq *modelsint.ReqBPostSOI,
	message globalmodels.Message) (*http.Response,
	globalmodels.Message,
	*globalmodels.ErrorGeneric) {
	// headers
	headersToken := http.Header{}
	headersToken.Add(configapp.ContentType, configapp.ContentTypeJSON)
	headersToken.Add("RqUID", message.HeaderReq.Get(configapp.XRqUID))
	headersToken.Add("Channel", message.HeaderReq.Get(configapp.XChannel))
	headersToken.Add("Backend", "ACH")
	headersToken.Add("ServiceName", "InformationInquiryPILA")
	headersToken.Add("OperationName", "getInformationPILA")
	headersToken.Add("Ref1", message.HeaderReq.Get(configapp.XIdentSerialNum))
	headersToken.Add("Ref2", message.HeaderReq.Get("XGovIssueIdentType"))
	headersToken.Add("BankId", message.HeaderReq.Get(configapp.XCompanyID))

	// Peticion para obtener el token
	var tokenOauthSOIRq modelsext.ReqBPostOauthSOI

	tokenOauthSOIRq.Tenant = historicRq.SecretList.CryptType
	tokenOauthSOIRq.Password = historicRq.SecretList.SecretID
	tokenOauthSOIRq.Email = historicRq.SecretList.Secret

	// url de consumo
	urlToken, err := url.Parse(os.Getenv("URL_TOKENSOI"))

	if err != nil {
		message.Status = http.StatusInternalServerError
		errorGeneric := globalmodels.BuildInternalServerError(err)

		return nil, message, &errorGeneric
	}
	dataToken, _ := json.Marshal(tokenOauthSOIRq)
	timeOut := date.TimeoutValue(os.Getenv("TIMEOUT_BACKEND"))

	logger.TracerLog(span, "SOIToken-PRq", "", urlToken, headersToken, tokenOauthSOIRq, configapp.MaskTokenSOIRq)
	responseToken, err := commons.ClientHTTP(dataToken, headersToken, urlToken, "POST", timeOut)

	if err != nil && responseToken == nil {
		var errorGeneric globalmodels.ErrorGeneric
		if strings.Contains(err.Error(), "Client.Timeout") {
			message.Status = http.StatusGatewayTimeout
			message.Description = configapp.TimeoutDescription
			errorGeneric = globalmodels.BuildTimeOutError(err)
		} else {
			message.Status = http.StatusInternalServerError
			message.Description = config.MsgInternalError
			errorGeneric = globalmodels.BuildInternalServerError(err)
		}
		return nil, message, &errorGeneric
	}
	return responseToken, message, nil
}

// postQuery obtiene el token de SOI
func postQuery(span context.Context, historicRq *modelsint.ReqBPostSOI, cookie string,
	message globalmodels.Message) (*http.Response,
	globalmodels.Message,
	*globalmodels.ErrorGeneric) {
	// headers
	headersQuery := http.Header{}
	headersQuery.Add(configapp.ContentType, configapp.ContentTypeJSON)
	headersQuery.Add("RqUID", message.HeaderReq.Get(configapp.XRqUID))
	headersQuery.Add("Channel", message.HeaderReq.Get(configapp.XChannel))
	headersQuery.Add("Backend", "ACH")
	headersQuery.Add("ServiceName", "InformationInquiryPILA")
	headersQuery.Add("OperationName", "getInformationPILA")
	headersQuery.Add("Ref1", message.HeaderReq.Get(configapp.XIdentSerialNum))
	headersQuery.Add("Ref2", message.HeaderReq.Get("XGovIssueIdentType"))
	headersQuery.Add("BankId", message.HeaderReq.Get(configapp.XCompanyID))
	headersQuery.Add("cookie", cookie)

	var uri, typeNumber string
	// url de consumo
	govIssueIdentType := message.HeaderReq.Get("XGovIssueIdentType")
	if strings.Contains(configapp.QueryNatural, govIssueIdentType) {
		uri = os.Getenv("URL_QUERYSOINATURAL")
	} else {
		uri = os.Getenv("URL_QUERYSOIJURIDICO")
	}
	typeNumber = message.HeaderReq.Get(configapp.XGovIssueIdentType) + message.HeaderReq.Get(configapp.XIdentSerialNum)
	uri = strings.ReplaceAll(uri, "{TypeNumber}", typeNumber)

	urlQuery, err := url.Parse(uri)

	if err != nil {
		message.Status = http.StatusInternalServerError
		errorGeneric := globalmodels.BuildInternalServerError(err)

		return nil, message, &errorGeneric
	}

	queryParam, _ := url.ParseQuery(urlQuery.RawQuery)
	queryParam.Add("historico", historicRq.RefInfo.RefType)
	urlQuery.RawQuery = queryParam.Encode()

	timeOut := date.TimeoutValue(os.Getenv("TIMEOUT_BACKEND"))

	logger.TracerLog(span, "SOIQuery-PRq", "", urlQuery, headersQuery, nil, nil)
	responseToken, err := commons.ClientHTTP(nil, headersQuery, urlQuery, "GET", timeOut)

	if err != nil && responseToken == nil {
		var errorGeneric globalmodels.ErrorGeneric
		if strings.Contains(err.Error(), "Client.Timeout") {
			message.Status = http.StatusGatewayTimeout
			message.Description = configapp.TimeoutDescription
			errorGeneric = globalmodels.BuildTimeOutError(err)
		} else {
			message.Status = http.StatusInternalServerError
			message.Description = config.MsgInternalError
			errorGeneric = globalmodels.BuildInternalServerError(err)
		}
		return nil, message, &errorGeneric
	}

	data, _ := ioutil.ReadAll(responseToken.Body)
	var inputMsg interface{}
	_ = json.Unmarshal(data, &inputMsg)

	responseToken.Body = ioutil.NopCloser(bytes.NewReader(data))
	logger.TracerLog(span, "SOIQuery-PRs",
		strconv.Itoa(responseToken.StatusCode), nil,
		responseToken.Header, inputMsg, nil)

	return responseToken, message, nil
}

func logout(span context.Context, historicRq *modelsint.ReqBPostSOI,
	cookie string, message globalmodels.Message) {
	// headers
	headersLogout := http.Header{}
	headersLogout.Add(configapp.ContentType, configapp.ContentTypeJSON)
	headersLogout.Add("RqUID", message.HeaderReq.Get(configapp.XRqUID))
	headersLogout.Add("Channel", message.HeaderReq.Get(configapp.XChannel))
	headersLogout.Add("Backend", "ACH")
	headersLogout.Add("ServiceName", "InformationInquiryPILA")
	headersLogout.Add("OperationName", "getInformationPILA")
	headersLogout.Add("Ref1", message.HeaderReq.Get(configapp.XIdentSerialNum))
	headersLogout.Add("Ref2", message.HeaderReq.Get("XGovIssueIdentType"))
	headersLogout.Add("BankId", message.HeaderReq.Get(configapp.XCompanyID))
	headersLogout.Add("cookie", cookie)

	var uri string
	// url de consumo
	uri = os.Getenv("URL_FINISHSESSION")
	uri = strings.ReplaceAll(uri, "{usuario}", historicRq.SecretList.Secret)

	urlLogout, _ := url.Parse(uri)

	timeOut := date.TimeoutValue(os.Getenv("TIMEOUT_BACKEND"))

	logger.TracerLog(span, "SOILogout-PRq", "", urlLogout, headersLogout, nil, nil)
	logout, _ := commons.ClientHTTP(nil, headersLogout, urlLogout, "GET", timeOut)
	defer logout.Body.Close()

	data, _ := ioutil.ReadAll(logout.Body)
	var inputMsg interface{}
	_ = json.Unmarshal(data, &inputMsg)
	logger.TracerLog(span, "SOILogout-PRs",
		strconv.Itoa(logout.StatusCode), nil,
		logout.Header, inputMsg, nil)
}
