package mocks

import (
	"net/http"
	"net/http/httptest"
	"time"
)

// ServerMockSOI simula el backend de mareigua
func ServerMockSOI() *httptest.Server {
	handler := http.NewServeMux()
	handler.HandleFunc("/token", mockToken)
	handler.HandleFunc("/query", mockQuery)
	handler.HandleFunc("/logout", mockLogout)
	handler.HandleFunc("/tokenTimeout", mockTokenTimeout)
	handler.HandleFunc("/queryTimeout", mockQueryTimeout)
	handler.HandleFunc("/tokenError", mockTokenError)
	handler.HandleFunc("/queryError", mockQueryError)
	handler.HandleFunc("/DatapowerError", mockDatapowerError)

	srv := httptest.NewServer(handler)

	return srv
}

func mockTokenError(w http.ResponseWriter, r *http.Request) {
	response := `{
		"message": "Usuario y contraseña no validos"
	}`
	w.Header().Add("Set-Cookie", "TokenPrueba")
	w.WriteHeader(http.StatusUnauthorized)
	_, _ = w.Write([]byte(response))
}
func mockTokenTimeout(w http.ResponseWriter, r *http.Request) {
	response := `{
		"tenant": "ef0002",
		"email": "oscar.romero@avaldigitallabs.com",
		"permissions": [
			"*:consultas.individual-natural.consultar",
			"*:consultas.masivas-natural.consultar"
		]
	}`
	time.Sleep(4 * time.Second)
	w.Header().Add("Set-Cookie", "TokenPrueba")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(response))
}

func mockToken(w http.ResponseWriter, r *http.Request) {
	response := `{
		"tenant": "ef0002",
		"email": "oscar.romero@avaldigitallabs.com",
		"permissions": [
			"*:consultas.individual-natural.consultar",
			"*:consultas.masivas-natural.consultar"
		]
	}`
	w.Header().Add("Set-Cookie", "TokenPrueba")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(response))
}

func mockQueryError(w http.ResponseWriter, r *http.Request) {
	response := `{
		"message": "Informacion no encontrada"
		}`
	w.WriteHeader(http.StatusNotFound)
	_, _ = w.Write([]byte(response))
}

func mockQuery(w http.ResponseWriter, r *http.Request) {
	response := `[
			{
				"razonSocial": "ARQUIDIOCESIS DE TUNJA",
				"tipoDocumentoAportante": "NI",
				"numeroDocumentoAportante": "0234",
				"codActividadEconomica": "9491",
				"desActividadEconomica": "Actividades de asociaciones religiosas.",
				"codClaseAportante": "B",
				"desClaseAportante": "EMPRESAS CON MENOS DE 200 COTIZANTES",
				"codTipoAportante": "04",
				"desTipoAportante": "AGREMIACIONES O ASOCIACIONES",
				"tipoDocumentoUsuario": "CC",
				"numeroDocumentoUsuario": "***5828",
				"primerNombre": "HIPOLITO",
				"segundoNombre": "",
				"primerApellido": "MOYANO",
				"segundoApellido": "ALBA",
				"periodoCotizacion": "2019-10",
				"codTipoCotizante": "16",
				"descTipoCotizante": "INDEPENDIENTE AGREMIADO O ASOCIADO",
				"codTipoPlanilla": "Y",
				"descTipoPlanilla": "PLANILLA INDEPENDIENTES EMPRESAS",
				"novedades": "",
				"salarioBasico": 828116,
				"salarioIntegral": "1",
				"ingresoBaseCotizacionPension": 828116,
				"ingresoBaseCotizacionSalud": 828116,
				"ingresoBaseCotizacionSubFamiliar": 0
			},
			{
				"razonSocial": "ARQUIDIOCESIS DE TUNJA",
				"tipoDocumentoAportante": "NI",
				"numeroDocumentoAportante": "0234",
				"codActividadEconomica": "9491",
				"desActividadEconomica": "Actividades de asociaciones religiosas.",
				"codClaseAportante": "B",
				"desClaseAportante": "EMPRESAS CON MENOS DE 200 COTIZANTES",
				"codTipoAportante": "04",
				"desTipoAportante": "AGREMIACIONES O ASOCIACIONES",
				"tipoDocumentoUsuario": "CC",
				"numeroDocumentoUsuario": "***5828",
				"primerNombre": "HIPOLITO",
				"segundoNombre": "",
				"primerApellido": "MOYANO",
				"segundoApellido": "ALBA",
				"periodoCotizacion": "2019-09",
				"codTipoCotizante": "16",
				"descTipoCotizante": "INDEPENDIENTE AGREMIADO O ASOCIADO",
				"codTipoPlanilla": "Y",
				"descTipoPlanilla": "PLANILLA INDEPENDIENTES EMPRESAS",
				"novedades": "",
				"salarioBasico": 828116,
				"salarioIntegral": "",
				"ingresoBaseCotizacionPension": 828116,
				"ingresoBaseCotizacionSalud": 828116,
				"ingresoBaseCotizacionSubFamiliar": 0
			}
		]`
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(response))
}

func mockQueryTimeout(w http.ResponseWriter, r *http.Request) {
	response := `[
			{
				"razonSocial": "ARQUIDIOCESIS DE TUNJA",
				"tipoDocumentoAportante": "NI",
				"numeroDocumentoAportante": "0234",
				"codActividadEconomica": "9491",
				"desActividadEconomica": "Actividades de asociaciones religiosas.",
				"codClaseAportante": "B",
				"desClaseAportante": "EMPRESAS CON MENOS DE 200 COTIZANTES",
				"codTipoAportante": "04",
				"desTipoAportante": "AGREMIACIONES O ASOCIACIONES",
				"tipoDocumentoUsuario": "CC",
				"numeroDocumentoUsuario": "***5828",
				"primerNombre": "HIPOLITO",
				"segundoNombre": "",
				"primerApellido": "MOYANO",
				"segundoApellido": "ALBA",
				"periodoCotizacion": "2019-10",
				"codTipoCotizante": "16",
				"descTipoCotizante": "INDEPENDIENTE AGREMIADO O ASOCIADO",
				"codTipoPlanilla": "Y",
				"descTipoPlanilla": "PLANILLA INDEPENDIENTES EMPRESAS",
				"novedades": "",
				"salarioBasico": 828116,
				"salarioIntegral": "",
				"ingresoBaseCotizacionPension": 828116,
				"ingresoBaseCotizacionSalud": 828116,
				"ingresoBaseCotizacionSubFamiliar": 0
			}
		]`
	time.Sleep(4 * time.Second)
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(response))
}

func mockLogout(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Set-Cookie", "TokenPrueba")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(nil)
}

func mockDatapowerError(w http.ResponseWriter, r *http.Request) {
	response := `{
		"StatusCode": "700",
		"StatusDesc": "Prueba",
		"ServerStatusCode": "700",
		"ServerStatusDesc": "Prueba",
		"RqUID": "12312",
		"DPObject": "MPG.back",
		"URLIn":  "testIn",
		"URLOut": "testOut"
	}`
	w.Header().Add("Set-Cookie", "TokenPrueba")
	w.WriteHeader(http.StatusInternalServerError)
	_, _ = w.Write([]byte(response))
}
