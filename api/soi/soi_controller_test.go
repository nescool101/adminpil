package soi

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/api/soi/mocks"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
	"github.com/bmizerany/assert"
	"github.com/gin-gonic/gin"
)

func TestSOIController_BadRequestHeaders(t *testing.T) {
	request := modelsint.ReqBPostSOI{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "15",
		},
	}

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	controller := NewPostSOIController(tracer)

	b, _ := json.Marshal(request)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/SOI", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/SOI", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)

}

func TestPostSOIController_BadRequestBody(t *testing.T) {

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	controller := NewPostSOIController(tracer)

	b, _ := json.Marshal(`{
		"SecretList": {
		  
		  "CryptType": "password",
	  
		},
		"RefInfo": {
		  "RefType": "4"
		}
	  }`)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/SOI", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	req.Header.Add("X-CompanyId", "0001")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/SOI", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)

}

func TestPostSOIController_OK(t *testing.T) {

	srv := mocks.ServerMockSOI()
	defer srv.Close()

	_ = os.Setenv("TIMEOUT_BACKEND", "8")
	_ = os.Setenv("URL_TOKENSOI", srv.URL+"/token")
	_ = os.Setenv("URL_QUERYSOINATURAL", srv.URL+"/query")
	_ = os.Setenv("URL_FINISHSESSION", srv.URL+"/logout")
	_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "15",
		},
	}

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	controller := NewPostSOIController(tracer)

	b, _ := json.Marshal(request)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/SOI", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	req.Header.Add("X-CompanyId", "0001")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/SOI", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)

}

func TestPostSOIController_InternalServer(t *testing.T) {

	request := modelsint.ReqBPostMareigua{
		SecretList: modelsint.SecretList{
			SecretID:  "CtpPQU^/C@vr",
			CryptType: "password",
			Secret:    "800143157",
		},
		RefInfo: modelsint.RefInfo{
			RefType: "1",
		},
		CurAmt: modelsint.CurAmt{
			Amt: 1000000,
		},
	}

	tracer, closer := logger.InitJaeger("API_InquiriesPila")
	defer closer.Close()

	controller := NewPostSOIController(tracer)

	b, _ := json.Marshal(request)

	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()

	ctx, router := gin.CreateTestContext(w)

	req, _ := http.NewRequest(http.MethodPost, "/Administration-PILAOperators/SOI", bytes.NewReader(b))
	req.Header.Add("X-GovIssueIdentType", "CC")
	req.Header.Add("X-IdentSerialNum", "10083463")
	req.Header.Add("X-RqUID", "10083463")
	req.Header.Add("X-Channel", "PB")
	req.Header.Add("X-CompanyId", "0001")
	ctx.Request = req
	router.POST("/Administration-PILAOperators/SOI", func(c *gin.Context) { controller.Controller(ctx) })
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusInternalServerError, w.Code)

}
