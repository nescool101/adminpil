package soi

import (
	"context"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
)

// IPostSOI interface for service PostSOI
type IPostSOI interface {
	PostSOIHandler(span context.Context,
		historicRq *modelsint.ReqBPostSOI,
		message globalmodels.Message) (*modelsint.ResBPostSOI,
		*globalmodels.Message,
		*globalmodels.ErrorGeneric)
}
