package soi

import (
	"net/http"
	"strconv"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/configapp"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

// Controller is struct for interface
type Controller struct {
	postSOIService IPostSOI
	tracer         opentracing.Tracer
}

// NewPostSOIController contructor of controller
func NewPostSOIController(tracer opentracing.Tracer) *Controller {
	return &Controller{
		NewPostSOIService(),
		tracer}
}

// Controller funcion de procesamieto de negocio para la consulta a SOI
func (p *Controller) Controller(c *gin.Context) {
	span := logger.TracerLogParent(p.tracer, "SOI", c.Request.Header, c.Request.Method)

	var valHeaders modelsint.ReqHPostHeaders
	if errH := c.ShouldBindHeader(&valHeaders); errH != nil {
		resp := globalmodels.BuildBadRequestError(errH)
		c.Header("X-ApprolvaId", "0")
		c.Header(configapp.XRqUID, c.Request.Header.Get(configapp.XRqUID))
		logger.TracerLog(span, configapp.TagLogSOIFRs, "400", nil, c.Writer.Header(), resp, nil)

		c.JSON(http.StatusBadRequest, resp)
		return
	}

	var historicRq *modelsint.ReqBPostSOI
	if err := c.ShouldBindJSON(&historicRq); err != nil {
		resp := globalmodels.BuildBadRequestError(err)
		c.Header("X-ApprovalId", "0")
		c.Header(configapp.XRqUID, c.Request.Header.Get(configapp.XRqUID))
		logger.TracerLog(span, configapp.TagLogSOIFRs, "400", nil, c.Writer.Header(), resp, nil)

		c.JSON(http.StatusBadRequest, resp)
		return
	}

	go logger.TracerLog(span, "SOI-FRq", "", c.Request.URL, c.Request.Header, historicRq, configapp.MaskSOIRq)

	var reqInfo globalmodels.Message

	reqInfo.HeaderReq = c.Request.Header
	reqInfo.Method = c.Request.Method

	response, resInfo, errorGeneric := p.postSOIService.PostSOIHandler(span, historicRq, reqInfo)

	for k, z := range resInfo.HeaderRes {
		c.Header(k, z)
	}

	if errorGeneric != nil {
		logger.TracerLog(span, configapp.TagLogSOIFRs, strconv.Itoa(resInfo.Status), nil, c.Writer.Header(), errorGeneric, nil)
		c.JSON(resInfo.Status, errorGeneric)
		return
	}

	logger.TracerLog(span, configapp.TagLogSOIFRs, strconv.Itoa(resInfo.Status), nil, c.Writer.Header(), response, nil)
	c.JSON(resInfo.Status, response)
}
