package soi

import (
	"context"
	"net/http"
	"os"
	"reflect"
	"testing"

	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/api/soi/mocks"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/modelsint"
	"github.com/avaldigitallabs/adl-sc-backend-library/globalmodels"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"
)

func TestPostSOI_PostSOIHandler(t *testing.T) {
	srv := mocks.ServerMockSOI()
	defer srv.Close()

	tracer, closer := logger.InitJaeger("AP_AdministrationPilaOperators")
	defer closer.Close()

	headers := http.Header{}
	headersNI := http.Header{}

	headers.Add("X-GovIssueIdentType", "CC")
	headers.Add("X-IdentSerialNum", "10083463")
	headers.Add("X-RqUID", "10083463")
	headers.Add("X-Channel", "PB")
	headers.Add("X-CompanyId", "0001")

	headersNI.Add("X-GovIssueIdentType", "NI")
	headersNI.Add("X-IdentSerialNum", "10083463")
	headersNI.Add("X-RqUID", "10083463")
	headersNI.Add("X-Channel", "PB")
	headersNI.Add("X-CompanyId", "0001")

	span := logger.TracerLogParent(tracer, "SOI", headers, "POST")

	soi := NewPostSOIService()

	tests := []struct {
		name       string
		span       context.Context
		historicRq *modelsint.ReqBPostSOI
		message    globalmodels.Message
		env        func()
		validate   int
		want       *modelsint.ResBPostSOI
		want1      int
		want2      *globalmodels.ErrorGeneric
	}{
		{
			name:       "Test/PostSOIHandlerOkCC",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "8")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/token")
				_ = os.Setenv("URL_QUERYSOINATURAL", srv.URL+"/query")
				_ = os.Setenv("URL_FINISHSESSION", srv.URL+"/logout")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 1,
			want:     bodyResponseOk,
			want1:    201,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerOkNI",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headersNI,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "8")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/token")
				_ = os.Setenv("URL_QUERYSOIJURIDICO", srv.URL+"/query")
				_ = os.Setenv("URL_FINISHSESSION", srv.URL+"/logout")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 1,
			want:     bodyResponseOk,
			want1:    201,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerTimeoutQuery",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "1")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/token")
				_ = os.Setenv("URL_QUERYSOINATURAL", srv.URL+"/queryTimeout")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 0,
			want:     nil,
			want1:    504,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerTimeoutToken",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "1")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/tokenTimeout")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 0,
			want:     nil,
			want1:    504,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerErrorToken",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "1")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/tokenError")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 0,
			want:     nil,
			want1:    206,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerErrorQuery",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "1")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/token")
				_ = os.Setenv("URL_QUERYSOINATURAL", srv.URL+"/queryError")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 0,
			want:     nil,
			want1:    206,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerUrlErrorQuery",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "1")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/token")
				_ = os.Setenv("URL_QUERYSOINATURAL", srv.URL+"?/queryError")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 0,
			want:     nil,
			want1:    503,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerUrlErrorToken",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "1")
				_ = os.Setenv("URL_TOKENSOI", "htt://91823:?")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 0,
			want:     nil,
			want1:    500,
			want2:    nil,
		},
		{
			name:       "Test/PostSOIHandlerErrorDatapower",
			span:       span,
			historicRq: bodyRequest,
			message: globalmodels.Message{
				HeaderReq: headers,
				Method:    "POST",
			},
			env: func() {
				_ = os.Setenv("TIMEOUT_BACKEND", "1")
				_ = os.Setenv("URL_TOKENSOI", srv.URL+"/DatapowerError")
				_ = os.Setenv("JAEGER_ENDPOINT", "http://localhost:14268/api/traces")
			},
			validate: 0,
			want:     nil,
			want1:    500,
			want2:    nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.env()
			got, got1, got2 := soi.PostSOIHandler(tt.span, tt.historicRq, tt.message)
			if tt.validate == 1 {
				if !reflect.DeepEqual(got, tt.want) {
					t.Errorf("PostSOI.PostSOIHandler() got2 = %v, want %v", got, tt.want)
				}
				if !reflect.DeepEqual(got2, tt.want2) {
					t.Errorf("PostSOI.PostSOIHandler() got2 = %v, want %v", got2, tt.want2)
				}
			}
			if got1.Status != tt.want1 {
				t.Errorf("PostSOI.PostSOIHandler() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

var bodyRequest = &modelsint.ReqBPostSOI{
	SecretList: modelsint.SecretList{
		SecretID:  "CtpPQU^/C@vr",
		CryptType: "password",
		Secret:    "800143157",
	},
	RefInfo: modelsint.RefInfo{
		RefType: "15",
	},
}

var bodyResponseOk = &modelsint.ResBPostSOI{
	FinancialInformation: []modelsint.FinancialInformation{
		{
			PersonInfo: &modelsint.PersonInfo{
				PersonName: &modelsint.PersonName{
					LastName:       "MOYANO",
					FirstName:      "HIPOLITO",
					SecondLastName: "ALBA",
				},
				GovIssueIdent: &modelsint.GovIssueIdent{
					GovIssueIdentType: "CC",
					IdentSerialNum:    "***5828",
				},
			},
			OrgInfo: &modelsint.OrgInfo{
				Name: "ARQUIDIOCESIS DE TUNJA",
			},
			OrgID: &modelsint.OrgID{
				OrgIDType: "NI",
				OrgIDNum:  "0234",
			},
			Code:  "B",
			Value: "EMPRESAS CON MENOS DE 200 COTIZANTES",
			BusinessActivity: &modelsint.BusinessActivity{
				BusinessType: "9491",
				Desc:         "Actividades de asociaciones religiosas.",
			},
			Issuer:      "16",
			Desc:        "INDEPENDIENTE AGREMIADO O ASOCIADO",
			EstablishDt: "2019-10",
			CurAmt: &modelsint.CurAmt{
				Amt: 828116,
			},
			Amt: 1,
			AdditionalData: []modelsint.AdditionalData{
				{
					Name:  "tipoCotizante",
					Value: "INDEPENDIENTE AGREMIADO O ASOCIADO",
				},
				{
					Name:  "tipoPlanillacodigo",
					Value: "Y",
				},
				{
					Name:  "tipoPlanillaDescripcion",
					Value: "PLANILLA INDEPENDIENTES EMPRESAS",
				},
				{
					Name:  "ingresoBaseCotizacionPension",
					Value: "828116",
				},
				{
					Name:  "ingresoBaseCotizacionSalud",
					Value: "828116",
				},
				{
					Name:  "ingresoBaseCotizacionSubsidioFamiliar",
					Value: "0",
				},
			},
		},
		{
			PersonInfo: &modelsint.PersonInfo{
				PersonName: &modelsint.PersonName{
					LastName:       "MOYANO",
					FirstName:      "HIPOLITO",
					SecondLastName: "ALBA",
				},
				GovIssueIdent: &modelsint.GovIssueIdent{
					GovIssueIdentType: "CC",
					IdentSerialNum:    "***5828",
				},
			},
			OrgInfo: &modelsint.OrgInfo{
				Name: "ARQUIDIOCESIS DE TUNJA",
			},
			OrgID: &modelsint.OrgID{
				OrgIDType: "NI",
				OrgIDNum:  "0234",
			},
			Code:  "B",
			Value: "EMPRESAS CON MENOS DE 200 COTIZANTES",
			BusinessActivity: &modelsint.BusinessActivity{
				BusinessType: "9491",
				Desc:         "Actividades de asociaciones religiosas.",
			},
			Issuer:      "16",
			Desc:        "INDEPENDIENTE AGREMIADO O ASOCIADO",
			EstablishDt: "2019-09",
			CurAmt: &modelsint.CurAmt{
				Amt: 828116,
			},
			Amt: 0,
			AdditionalData: []modelsint.AdditionalData{
				{
					Name:  "tipoCotizante",
					Value: "INDEPENDIENTE AGREMIADO O ASOCIADO",
				},
				{
					Name:  "tipoPlanillacodigo",
					Value: "Y",
				},
				{
					Name:  "tipoPlanillaDescripcion",
					Value: "PLANILLA INDEPENDIENTES EMPRESAS",
				},
				{
					Name:  "ingresoBaseCotizacionPension",
					Value: "828116",
				},
				{
					Name:  "ingresoBaseCotizacionSalud",
					Value: "828116",
				},
				{
					Name:  "ingresoBaseCotizacionSubsidioFamiliar",
					Value: "0",
				},
			},
		},
	},
}

var responseTimeout = globalmodels.ErrorGeneric{
	MsgRsHdr: globalmodels.MsgRsHdr{
		Status: globalmodels.Status{
			StatusCode:       "504",
			StatusDesc:       "Client.Timeout",
			Severity:         "Tiempo de espera para la respuesta superado",
			ServerStatusCode: "Client.Timeout",
			ServerStatusDesc: "Post \"https://10.133.6.8:6902/administracion/auth/token\": context deadline exceeded (Client.Timeout exceeded while awaiting headers)",
			EndDt:            "2020-07-28T14:57:13",
		},
	},
}
