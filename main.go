package main

import "github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/app"

func main() {
	app.InitApp()
}
