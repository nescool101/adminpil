package modelsext

// ReqBPostOauthSOI request to obtain the Oauth token
type ReqBPostOauthSOI struct {
	Tenant   string `json:"tenant,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}
