package modelsext

// ResBPostOauthMareigua token response obtained
type ResBPostOauthMareigua struct {
	AccessToken string `json:"access_token,omitempty"`
	TokenType   string `json:"token_type,omitempty"`
	ExpiresIn   string `json:"expires_in,omitempty"`
}
