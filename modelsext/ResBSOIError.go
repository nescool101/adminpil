package modelsext

// ResBSOIError estructura de error
type ResBSOIError struct {
	Message          string `json:"message,omitempty"`
	StatusCode       string `json:"StatusCode,omitempty"`
	StatusDesc       string `json:"StatusDesc,omitempty"`
	ServerStatusCode string `json:"ServerStatusCode,omitempty"`
	ServerStatusDesc string `json:"ServerStatusDesc,omitempty"`
	RqUID            string `json:"RqUID,omitempty"`
	DPObject         string `json:"DPObject,omitempty"`
	URLIn            string `json:"URLIn,omitempty"`
	URLOut           string `json:"URLOut,omitempty"`
}
