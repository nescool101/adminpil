package modelsext

// ReqBPostQueryMareigua request for consultation to the tidal service
type ReqBPostQueryMareigua struct {
	TipoIdentificacionID string  `json:"tipo_identificacion_id,omitempty"`
	NumeroEmpleadoID     string  `json:"NumeroEmpleado_id,omitempty"`
	SalarioMensual       float32 `json:"SalarioMensual,omitempty"`
	ProductoID           int     `json:"producto_id,omitempty"`
}
