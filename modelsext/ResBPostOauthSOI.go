package modelsext

// ResBPostOauthSOI token response obtained
type ResBPostOauthSOI struct {
	Tenant      string   `json:"tenant,omitempty"`
	Email       string   `json:"email,omitempty"`
	Permissions []string `json:"permissions,omitempty"`
}
