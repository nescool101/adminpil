package modelsext

// ResBPostQuerySOI information of the companies or people who have made contributions to the client
type ResBPostQuerySOI struct {
	RazonSocial                      string  `json:"razonSocial,omitempty"`
	NumeroDocumentoAportante         string  `json:"numeroDocumentoAportante,omitempty"`
	CodActividadEconomica            string  `json:"codActividadEconomica,omitempty"`
	TipoDocumentoAportante           string  `json:"tipoDocumentoAportante,omitempty"`
	DesActividadEconomica            string  `json:"desActividadEconomica,omitempty"`
	CodClaseAportante                string  `json:"codClaseAportante,omitempty"`
	DesClaseAportante                string  `json:"desClaseAportante,omitempty"`
	CodTipoAportante                 string  `json:"codTipoAportante,omitempty"`
	DesTipoAportante                 string  `json:"desTipoAportante,omitempty"`
	TipoDocumentoUsuario             string  `json:"tipoDocumentoUsuario,omitempty"`
	NumeroDocumentoUsuario           string  `json:"numeroDocumentoUsuario,omitempty"`
	PrimerNombre                     string  `json:"primerNombre,omitempty"`
	SegundoNombre                    string  `json:"segundoNombre,omitempty"`
	PrimerApellido                   string  `json:"primerApellido,omitempty"`
	SegundoApellido                  string  `json:"segundoApellido,omitempty"`
	PeriodoCotizacion                string  `json:"periodoCotizacion,omitempty"`
	CodTipoCotizante                 string  `json:"codTipoCotizante,omitempty"`
	DescTipoCotizante                string  `json:"descTipoCotizante,omitempty"`
	Ingreso                          string  `json:"Ingreso,omitempty"`
	Retiro                           string  `json:"Retiro,omitempty"`
	CodTipoPlanilla                  string  `json:"codTipoPlanilla,omitempty"`
	DescTipoPlanilla                 string  `json:"descTipoPlanilla,omitempty"`
	Novedades                        string  `json:"novedades,omitempty"`
	SalarioBasico                    float32 `json:"salarioBasico,omitempty"`
	SalarioIntegral                  string  `json:"salarioIntegral,omitempty"`
	IngresoBaseCotizacionPension     int     `json:"ingresoBaseCotizacionPension,omitempty"`
	IngresoBaseCotizacionSalud       int     `json:"ingresoBaseCotizacionSalud,omitempty"`
	IngresoBaseCotizacionSubFamiliar int     `json:"ingresoBaseCotizacionSubFamiliar,omitempty"`
}
