package modelsext

// ResBMareiguaError estructura de error
type ResBMareiguaError struct {
	Error            string `json:"error,omitempty"`
	ErrorDescription string `json:"error_description,omitempty"`
	Message          string `json:"message,omitempty"`
}
