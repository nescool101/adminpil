package modelsext

// ResBPostQueryMareigua tidal service response message
type ResBPostQueryMareigua struct {
	Fecha                              string       `json:"fecha,omitempty"`
	ConsultaID                         int          `json:"consulta_id,omitempty"`
	RespuestaID                        int          `json:"respuesta_id,omitempty"`
	TipoIdentificacionPersonaNaturalID int          `json:"tipo_identificacion_persona_natural_id,omitempty"`
	NumeroIdentificacionPersonaNatural string       `json:"numero_identificacion_persona_natural,omitempty"`
	PrimerNombrePersonaNatural         string       `json:"primer_nombre_persona_natural,omitempty"`
	SegundoNombrePersonaNatural        string       `json:"segundo_nombre_persona_natural,omitempty"`
	PrimerApellidoPersonaNatural       string       `json:"primer_apellido_persona_natural,omitempty"`
	SegundoApellidoPersonaNatural      string       `json:"segundo_apellido_persona_natural,omitempty"`
	EPS                                string       `json:"EPS,omitempty"`
	AFP                                string       `json:"AFP,omitempty"`
	Aportantes                         []Aportantes `json:"aportantes,omitempty"`
	ResultadoSalario                   *bool        `json:"resultado_salario,omitempty"`
	ResultadoSalarioZonaGris           *bool        `json:"resultado_salario_zona_gris,omitempty"`
	RangoSalario                       string       `json:"rango_salario,omitempty"`
}

// Aportantes information of the companies or people who have made contributions to the client
type Aportantes struct {
	TipoIdentificacionAportanteID   int              `json:"tipo_identificacion_aportante_id,omitempty"`
	NumeroIdentificacionAportante   string           `json:"numero_identificacion_aportante,omitempty"`
	RazónSocialAportante            string           `json:"razón_social_aportante,omitempty"`
	TipoCotizantePersonaNatural     int              `json:"tipo_cotizante_persona_natural,omitempty"`
	TieneSalarioIntegralActualmente *bool            `json:"tiene_salario_integral_actualmente,omitempty"`
	PromedioIngresos                float32          `json:"promedio_ingresos,omitempty"`
	MediaIngresos                   float32          `json:"media_ingresos,omitempty"`
	ResultadoPagos                  []ResultadoPagos `json:"resultado_pagos,omitempty"`
}

// ResultadoPagos information about payments made
type ResultadoPagos struct {
	AnoPeriodoValidado int     `json:"ano_periodo_validado,omitempty"`
	MesPeriodoValidado int     `json:"mes_periodo_validado,omitempty"`
	RealizoPago        *bool   `json:"realizo_pago,omitempty"`
	Ingresos           float32 `json:"ingresos,omitempty"`
}
