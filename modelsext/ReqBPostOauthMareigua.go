package modelsext

// ReqBPostOauthMareigua request to obtain the Oauth token
type ReqBPostOauthMareigua struct {
	GrantType string `json:"grant_type,omitempty"`
	Username  string `json:"username,omitempty"`
	Password  string `json:"password,omitempty"`
}
