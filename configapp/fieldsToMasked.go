package configapp

// MaskMareiguaRq const mask body
var MaskMareiguaRq = map[string]map[string]string{
	"body": {"SecretList/SecretId": "total",
		"SecretList/Secret": "partial",
	},
}

// MaskBTokenMareiguaRq const mask body
var MaskBTokenMareiguaRq = map[string]map[string]string{
	"body": {"password": "total",
		"username": "partial",
	},
}

// MaskBTokenMareiguaRs constante de campo a ofuscar
var MaskBTokenMareiguaRs = map[string]map[string]string{
	"body": {"password": "total",
		"username": "partial",
	},
}

// MaskSOIRq const mask body
var MaskSOIRq = map[string]map[string]string{
	"body": {"SecretList/SecretId": "total",
		"SecretList/Secret": "partial",
	},
}

// MaskTokenSOIRq const mask body
var MaskTokenSOIRq = map[string]map[string]string{
	"body": {"password": "total",
		"email": "partial",
	},
}

// MaskTokenSOIRs const mask body
var MaskTokenSOIRs = map[string]map[string]string{
	"body": {
		"email": "partial",
	},
}
