package configapp

// ErrorDescription Homologacion para los codigos de respuesta
var ErrorDescription = map[int]string{
	1:  "No contiene información de la persona natural",
	2:  "Datos erróneos. No cumple con el tipo de dato requerido",
	3:  "Error de forma. No cumple con el estándar de parámetros",
	4:  "Exitosa",
	5:  "Datos Incompletos",
	6:  "Falló la comunicación con los operadores de información",
	7:  "Error en el servidor",
	11: "Consulta en ambiente no autorizado",
}

// MsgInternalError descripcion de error
const MsgInternalError = "Internal Error"

// MsgSeverityError descripcion de error
const MsgSeverityError = "Error"
