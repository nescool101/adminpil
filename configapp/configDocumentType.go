package configapp

// DocumentTypeRq Homologacion para tipos de documento peticion
var DocumentTypeRq = map[string]string{
	"CC": "1",
	"CE": "2",
	"NI": "3",
	"RC": "5",
	"TI": "4",
	"PA": "6",
}

// DocumentTypeRs Homologacion para tipos de documento respuesta
var DocumentTypeRs = map[int]string{
	1: "CC",
	2: "CE",
	3: "NI",
	4: "TI",
	5: "RC",
	6: "PA",
}
