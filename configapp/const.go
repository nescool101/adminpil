package configapp

//  name tag for log
const (
	TagLogMareiguaFRs  = "Mareigua-FRs"
	TagLogSOIFRs       = "SOI-FRs"
	ContentType        = "Content-Type"
	ContentTypeForm    = "application/x-www-form-urlencoded"
	ContentTypeJSON    = "application/json"
	TimeoutDescription = "Tiempo de espera para la respuesta superado"
	XRqUID             = "X-RqUID"
	XChannel           = "X-Channel"
	XIdentSerialNum    = "X-IdentSerialNum"
	XCompanyID         = "X-CompanyId"
	XGovIssueIdentType = "X-GovIssueIdentType"
	QueryLegal         = "NI,NE"
	DescServiceUna     = "Service Unavailable"
	DescServiceError   = "Service invocation error"
)

var QueryNatural = "CC,CE,RC,TI,CD,PA"
