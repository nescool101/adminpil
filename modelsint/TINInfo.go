package modelsint

// TINInfo tax payment information
type TINInfo struct {
	TINType  string `json:"TINType,omitempty"`
	TaxID    string `json:"TaxId,omitempty"`
	CertCode string `json:"CertCode,omitempty"`
}
