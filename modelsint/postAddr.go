package modelsint

// PostAddr residence information
type PostAddr struct {
	Addr1      string `json:"Addr1,omitempty"`
	Addr2      string `json:"Addr2,omitempty"`
	Addr3      string `json:"Addr3,omitempty"`
	Addr4      string `json:"Addr4,omitempty"`
	City       string `json:"City,omitempty"`
	StateProv  string `json:"StateProv,omitempty"`
	PostalCode string `json:"PostalCode,omitempty"`
	Country    string `json:"Country,omitempty"`
	AddrType   string `json:"AddrType,omitempty"`
	StartDt    string `json:"StartDt,omitempty"`
	EndDt      string `json:"EndDt,omitempty"`
}
