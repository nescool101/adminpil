package modelsint

// ReqBPostSOI PILA payment history information request request
type ReqBPostSOI struct {
	SecretList SecretList `json:"SecretList,omitempty"`
	RefInfo    RefInfo    `json:"RefInfo,omitempty"`
	CurAmt     CurAmt     `json:"CurAmt,omitempty"`
}
