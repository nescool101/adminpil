package modelsint

// ResBPostSOI query response information contribution in SOI
type ResBPostSOI struct {
	FinancialInformation []FinancialInformation `json:"FinancialInformation,omitempty"`
}
