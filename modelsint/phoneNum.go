package modelsint

// PhoneNum phone number information
type PhoneNum struct {
	PhoneType string `json:"PhoneType,omitempty"`
	Phone     int    `json:"Phone,omitempty"`
}
