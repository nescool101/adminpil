package modelsint

// SalaryValidation validation salary information
type SalaryValidation struct {
	SalaryResult         *bool  `json:"SalaryResult,omitempty"`
	GrayZoneSalaryResult *bool  `json:"GrayZoneSalaryResult,omitempty"`
	SalaryRange          string `json:"SalaryRange,omitempty"`
}
