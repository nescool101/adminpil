package modelsint

// OrgID information the organization
type OrgID struct {
	OrgIDType   string `json:"OrgIdType,omitempty"`
	OrgIDNum    string `json:"OrgIdNum,omitempty"`
	OptOrgIDNum string `json:"OptOrgIdNum,omitempty"`
}
