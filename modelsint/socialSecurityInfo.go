package modelsint

// SocialSecurityInfo information on affiliation to the social security system
type SocialSecurityInfo struct {
	EPSName string `json:"EPSName,omitempty"`
	AFPName string `json:"AFPName,omitempty"`
}
