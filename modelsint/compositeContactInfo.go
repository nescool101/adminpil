package modelsint

// CompositeContactInfo information the contact
type CompositeContactInfo struct {
	ContactInfoType string       `json:"ContactInfoType,omitempty"`
	ContactInfo     *ContactInfo `json:"ContactInfo,omitempty"`
}
