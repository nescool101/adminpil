package modelsint

// OrgInfo information company
type OrgInfo struct {
	IndustID             *IndustID             `json:"IndustId,omitempty"`
	Name                 string                `json:"Name,omitempty"`
	LegalName            string                `json:"LegalName,omitempty"`
	CompositeContactInfo *CompositeContactInfo `json:"CompositeContactInfo,omitempty"`
	TINInfo              *TINInfo              `json:"TINInfo,omitempty"`
	EstablishDt          string                `json:"EstablishDt,omitempty"`
	NumEmployees         int                   `json:"NumEmployees,omitempty"`
	ContributionData     *ContributionData     `json:"ContributionData,omitempty"`
	Image                *Image                `json:"Image,omitempty"`
}
