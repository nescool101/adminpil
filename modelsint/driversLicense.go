package modelsint

// DriversLicense driving license information
type DriversLicense struct {
	LicenseNum string `json:"LicenseNum,omitempty"`
	StateProv  string `json:"StateProv,omitempty"`
	Country    string `json:"Country,omitempty"`
}
