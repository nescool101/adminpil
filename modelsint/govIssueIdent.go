package modelsint

// GovIssueIdent Customer information
type GovIssueIdent struct {
	GovIssueIdentType string `json:"GovIssueIdentType,omitempty"`
	IdentSerialNum    string `json:"IdentSerialNum,omitempty"`
	GovOrg            string `json:"GovOrg,omitempty"`
	GovOrgName        string `json:"GovOrgName,omitempty"`
	GovRank           string `json:"GovRank,omitempty"`
	StateProv         string `json:"StateProv,omitempty"`
	Country           string `json:"Country,omitempty"`
	Desc              string `json:"Desc,omitempty"`
	IssDt             string `json:"IssDt,omitempty"`
	ExpDt             string `json:"ExpDt,omitempty"`
}
