package modelsint

// ResBPostMareigua query response information contribution in mareigua
type ResBPostMareigua struct {
	*PersonInfo       `json:"PersonInfo,omitempty"`
	Contribution      []Contribution `json:"Contribution,omitempty"`
	*SalaryValidation `json:"SalaryValidation,omitempty"`
}
