package modelsint

// IndustID type industric
type IndustID struct {
	Org       string `json:"Org,omitempty"`
	IndustNum string `json:"IndustNum,omitempty"`
}
