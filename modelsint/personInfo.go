package modelsint

// PersonInfo detailed customer information
type PersonInfo struct {
	NameAddrType        string `json:"NameAddrType,omitempty"`
	FullName            string `json:"FullName,omitempty"`
	*PersonName         `json:"PersonName,omitempty"`
	*ContactInfo        `json:"ContactInfo,omitempty"`
	*TINInfo            `json:"TINInfo,omitempty"`
	BirthDt             string `json:"BirthDt,omitempty"`
	*DriversLicense     `json:"DriversLicense,omitempty"`
	MotherMaidenName    string `json:"MotherMaidenName,omitempty"`
	SpouseName          string `json:"SpouseName,omitempty"`
	Gender              string `json:"Gender,omitempty"`
	MaritalStatus       string `json:"MaritalStatus,omitempty"`
	Dependents          int    `json:"Dependents,omitempty"`
	OEDCode             string `json:"OEDCode,omitempty"`
	*SocialSecurityInfo `json:"SocialSecurityInfo,omitempty"`
	*Passport           `json:"Passport,omitempty"`
	*GovIssueIdent      `json:"GovIssueIdent,omitempty"`
	*OtherIdentDoc      `json:"OtherIdentDoc,omitempty"`
}
