package modelsint

// PersonName Name of individual
type PersonName struct {
	LastName       string `json:"LastName,omitempty"`
	FirstName      string `json:"FirstName,omitempty"`
	MiddleName     string `json:"MiddleName,omitempty"`
	SecondLastName string `json:"SecondLastName,omitempty"`
	TitlePrefix    string `json:"TitlePrefix,omitempty"`
	NameSuffix     string `json:"NameSuffix,omitempty"`
	Nickname       string `json:"Nickname,omitempty"`
	LegalName      string `json:"LegalName,omitempty"`
	FullName       string `json:"FullName,omitempty"`
}
