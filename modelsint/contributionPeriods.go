package modelsint

// ContributionPeriods information on paid periods
type ContributionPeriods struct {
	ExpYear    string `json:"ExpYear,omitempty"`
	ExpMonth   string `json:"ExpMonth,omitempty"`
	*CurAmt    `json:"CurAmt,omitempty"`
	PaymentInd *bool `json:"PaymentInd,omitempty"`
}
