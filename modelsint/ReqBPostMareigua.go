package modelsint

// ReqBPostMareigua PILA payment history information request request
type ReqBPostMareigua struct {
	SecretList SecretList `json:"SecretList,omitempty" binding:"required"`
	RefInfo    RefInfo    `json:"RefInfo,omitempty" binding:""`
	CurAmt     CurAmt     `json:"CurAmt,omitempty" binding:""`
}
