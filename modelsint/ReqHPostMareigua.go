package modelsint

// ReqHPostHeaders structure to validate the mandatory headers
type ReqHPostHeaders struct {
	RqUID             string `header:"X-RqUID" binding:"required"`
	Channel           string `header:"X-Channel" binding:"required"`
	CompanyID         string `header:"X-CompanyId" binding:"required"`
	IdentSerialNum    string `header:"X-IdentSerialNum" binding:"required"`
	GovIssueIdentType string `header:"X-GovIssueIdentType" binding:"required"`
}
