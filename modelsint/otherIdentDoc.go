package modelsint

// OtherIdentDoc information from other types of documents
type OtherIdentDoc struct {
	Desc           string `json:"Desc,omitempty"`
	IdentSerialNum string `json:"IdentSerialNum,omitempty"`
	Issuer         string `json:"Issuer,omitempty"`
	IssueLoc       string `json:"IssueLoc,omitempty"`
	IssDt          string `json:"IssDt,omitempty"`
	ExpDt          string `json:"ExpDt,omitempty"`
}
