package modelsint

// SecretList security data
type SecretList struct {
	SecretID  string `json:"SecretId,omitempty" binding:"required"`
	CryptType string `json:"CryptType,omitempty" binding:"required"`
	Secret    string `json:"Secret,omitempty" binding:"required"`
}
