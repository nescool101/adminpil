package modelsint

// RefInfo information on the type of query
type RefInfo struct {
	RefType string `json:"RefType,omitempty"`
	RefID   string `json:"RefId,omitempty"`
}
