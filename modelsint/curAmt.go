package modelsint

// CurAmt information on currency rates
type CurAmt struct {
	Amt            float32 `json:"Amt,omitempty"`
	CurCode        string  `json:"CurCode,omitempty"`
	CurRate        float32 `json:"CurRate,omitempty"`
	CurConvertRule string  `json:"CurConvertRule,omitempty"`
}
