package modelsint

// ContributionData personal information of the person making the contribution
type ContributionData struct {
	Code string `json:"Code,omitempty"`
	Desc string `json:"Desc,omitempty"`
}
