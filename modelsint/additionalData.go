package modelsint

// AdditionalData information additional
type AdditionalData struct {
	Name  string `json:"Name,omitempty"`
	Value string `json:"Value,omitempty"`
}
