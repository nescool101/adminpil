package modelsint

// BusinessActivity type the activity
type BusinessActivity struct {
	BusinessType string `json:"BusinessType,omitempty"`
	Desc         string `json:"Desc,omitempty"`
	CertCode     string `json:"CertCode,omitempty"`
	Income       string `json:"Income,omitempty"`
	Expenses     string `json:"Expenses,omitempty"`
}
