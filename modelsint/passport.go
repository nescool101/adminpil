package modelsint

// Passport passport information
type Passport struct {
	PassportNumber  string `json:"PassportNumber,omitempty"`
	PassportCountry string `json:"PassportCountry,omitempty"`
	IssueLoc        string `json:"IssueLoc,omitempty"`
	IssDt           string `json:"IssDt,omitempty"`
	ExpDt           string `json:"ExpDt,omitempty"`
}
