package modelsint

// ContactInfo contact data information
type ContactInfo struct {
	ContactPref      string `json:"ContactPref,omitempty"`
	PrefTimeStart    string `json:"PrefTimeStart,omitempty"`
	PrefTimeEnd      string `json:"PrefTimeEnd,omitempty"`
	*PhoneNum        `json:"PhoneNum,omitempty"`
	ContactName      string `json:"ContactName,omitempty"`
	EmailAddr        string `json:"EmailAddr,omitempty"`
	URL              string `json:"URL,omitempty"`
	*ActivationPhone `json:"ActivationPhone,omitempty"`
	*PostAddr        `json:"PostAddr,omitempty"`
	ContactStatus    string `json:"ContactStatus,omitempty"`
}
