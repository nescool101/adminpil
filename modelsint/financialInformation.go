package modelsint

// FinancialInformation validation salary information
type FinancialInformation struct {
	PersonInfo       *PersonInfo       `json:"PersonInfo,omitempty"`
	OrgInfo          *OrgInfo          `json:"OrgInfo,omitempty"`
	OrgID            *OrgID            `json:"OrgId,omitempty"`
	Code             string            `json:"Code,omitempty"`
	Value            string            `json:"Value,omitempty"`
	BusinessActivity *BusinessActivity `json:"BusinessActivity,omitempty"`
	Issuer           string            `json:"Issuer,omitempty"`
	Desc             string            `json:"Desc,omitempty"`
	EstablishDt      string            `json:"EstablishDt,omitempty"`
	CurAmt           *CurAmt           `json:"CurAmt,omitempty"`
	Amt              int               `json:"Amt"`
	AdditionalData   []AdditionalData  `json:"AdditionalData,omitempty"`
}
