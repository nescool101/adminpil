package modelsint

// Contribution information of the companies or people who made contributions
type Contribution struct {
	*PersonName         `json:"PersonName,omitempty"`
	*GovIssueIdent      `json:"GovIssueIdent,omitempty"`
	*ContributionData   `json:"ContributionData,omitempty"`
	IntegralSalary      *bool                 `json:"IntegralSalary,omitempty"`
	ContributionPeriods []ContributionPeriods `json:"ContributionPeriods,omitempty"`
	AverageCurAmt       *CurAmt               `json:"AverageCurAmt,omitempty"`
	MediamCurAmt        *CurAmt               `json:"MediamCurAmt,omitempty"`
}
