package modelsint

// Image information the image
type Image struct {
	URL             string           `json:"URL,omitempty"`
	EmailAddr       string           `json:"EmailAddr,omitempty"`
	PostAddr        *PostAddr        `json:"PostAddr,omitempty"`
	ActivationPhone *ActivationPhone `json:"ActivationPhone,omitempty"`
}
