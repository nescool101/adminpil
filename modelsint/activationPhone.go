package modelsint

// ActivationPhone active phone
type ActivationPhone struct {
	PhoneNumber string `json:"PhoneNumber,omitempty"`
}
