package app

import (
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/api/health"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/api/mareigua"
	"github.com/avaldigitallabs/adl-sc-backend-administration-pilaoperators/api/soi"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

// InitRouter start the routes
func InitRouter(tracer opentracing.Tracer) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()
	r.Use(gin.Recovery())

	r.GET("/Health", health.GetController)
	v1 := r.Group("/v1")

	v1.POST("/Administration-PilaOperators/SOI", soi.NewPostSOIController(tracer).Controller)
	v1.POST("/Administration-PilaOperators/Mareigua", mareigua.NewPostMareiguaController(tracer).Controller)

	return r
}
