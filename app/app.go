package app

import (
	"log"
	"net/http"
	"os"

	"github.com/avaldigitallabs/adl-sc-backend-library/date"
	"github.com/avaldigitallabs/adl-sc-backend-library/logger"

	"github.com/joho/godotenv"
)

// InitApp inicia el servidor
func InitApp() {
	err := godotenv.Load()

	if err != nil && os.Getenv("SCOPE") == "dev" {
		log.Println("Error loading .env file", err)
	}

	tracer, closer := logger.InitJaeger("AP_AdministrationPilaOperators")
	defer closer.Close()
	route := InitRouter(tracer)
	timeOut := date.TimeoutValue(os.Getenv("TIMEOUT_EXPOSITION"))

	server := &http.Server{
		Addr:         os.Getenv("PUERTO"),
		Handler:      route,
		ReadTimeout:  timeOut,
		IdleTimeout:  timeOut,
		WriteTimeout: timeOut,
	}

	log.Println("Listening...")
	log.Println(server.ListenAndServe())
}
